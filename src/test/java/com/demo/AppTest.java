package com.demo;

import static org.junit.Assert.assertTrue;

import com.alibaba.fastjson.JSON;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        Map<String, String> hbaseConfigMap = new HashMap<>();
        hbaseConfigMap.put("hbase.zookeeper.property.clientPort", "2181");
        hbaseConfigMap.put("hbase.zookeeper.quorum", "node14.example.com,node15.example.com,node16.example.com" );
        hbaseConfigMap.put("zookeeper.znode.parent", "/hbase-unsecure");
    
        Connection hbaseConnection = HbaseUtil.getHbaseConnection(hbaseConfigMap);
        try {
            Admin admin = hbaseConnection.getAdmin();
            NamespaceDescriptor[] namespaceDescriptors = admin.listNamespaceDescriptors();
            System.out.println(JSON.toJSONString(namespaceDescriptors));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        HbaseUtil.closeConnection(hbaseConnection);
    }
}
