package com.demo;

import com.alibaba.fastjson.JSON;
import org.apache.hadoop.hbase.NamespaceDescriptor;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
    
        Map<String, String> hbaseConfigMap = new HashMap<>();
        hbaseConfigMap.put("hbase.zookeeper.property.clientPort", "2181");
        hbaseConfigMap.put("hbase.zookeeper.quorum", "node14.example.com,node15.example.com,node16.example.com" );
        hbaseConfigMap.put("zookeeper.znode.parent", "/hbase-unsecure");
    
        Connection hbaseConnection = HbaseUtil.getHbaseConnection(hbaseConfigMap);
        try {
            Admin admin = hbaseConnection.getAdmin();
            NamespaceDescriptor[] namespaceDescriptors = admin.listNamespaceDescriptors();
            System.out.println(JSON.toJSONString(namespaceDescriptors));
        } catch (IOException e) {
            e.printStackTrace();
        }
    
    
        System.out.println("Hello World!");
    }
}
