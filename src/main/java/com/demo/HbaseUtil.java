package com.demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;
import java.util.Map;

/**
 * @author : hdy
 * @date : 2019/7/8 16:16
 */
public class HbaseUtil {
    
    public static Connection getHbaseConnection(Map<String, String> hbaseConfigMap) {
        Configuration configuration = HBaseConfiguration.create();
        for (Map.Entry<String, String> stringStringEntry : hbaseConfigMap.entrySet()) {
            configuration.set(stringStringEntry.getKey(),stringStringEntry.getValue());
        }
    
        Connection connection = null;
        try {
            connection = ConnectionFactory.createConnection(configuration);
        } catch (IOException e) {
            closeConnection(connection);
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return connection;
    }
    
    public static void closeConnection(Connection connection) {
        try {
            if(null != connection && !connection.isClosed()) {
                connection.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
}
